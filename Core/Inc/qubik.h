/*
 *  Qubik: An open source 5x5x5 pico-satellite
 *
 *  Copyright (C) 2020, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef QUBIK_H_
#define QUBIK_H_

#include "ft_storage.h"
#include "radio.h"
#include "max17261_driver.h"
#include "power.h"
#include "antenna.h"
#include "fsm.h"
#include "conf.h"
#include "sha256.h"

/**
 * @brief struct to store previous reasons of reset
 * Includes counters for each individual candidate reason
 */
struct ror_counters {
	// Counter of resets caused by low power
	uint16_t low_power_counter;
	// Counter of resets caused by independent watchdog
	uint16_t independent_watchdog_counter;
	// Counter of resets caused by software
	uint16_t software_counter;
	// Counter of resets caused by brownout
	uint16_t brownout_counter;
};

/**
 * @brief settings stored in flash
 * Align 32- and 16-bit values on respective borders to enable packing without
 * unaligned access penalties.
 * Size of this struct should not exceed 223 bytes
 *
 * ATTENTION: remember to change the value of QUBIK_FLASH_MAGIC_VAL in conf.h to
 * a different unique random value when changing this structure!
 */
// TODO: make __attribute__((packed)), for now causes a compiler warning
struct qubik_settings {
	uint32_t init;
	uint16_t scid;				//!< CCSDS Spacecraft identifier
	uint32_t sync_word;
	struct ror_counters reset_counters;
	uint8_t first_deploy;			//!< First deployment True/False
	uint8_t antenna_first_deploy;		//!< First antenna deployment True/False
	uint16_t holdoff_timer;			//!< Post deploy hold of in seconds
	uint32_t tx_freq;
	uint32_t rx_freq;
	uint16_t telemetry_map[QUBIK_TELEMETRY_MAP_LEN];
	struct ant_status antenna_status;
	struct ant_power_stats antenna_pwr_stats;
	radio_encoding_t tm_resp_enc;
	radio_modulation_t tm_resp_mod;
	radio_baud_t tm_resp_baud;
	uint32_t rildos_gold_seed;
	uint8_t sha256[SHA_CHECK_SIZE];
	uint16_t trx_delay_ms;			//!< Delay in ms for switching from RX to TX
	uint8_t mute_flag;
	int8_t tm_resp_tx_power;
	uint8_t tm_resp_pa_en;
};

/**
 * @brief Hardware status bits
 */
struct qubik_hw_status {
	uint8_t power_save;		//!< Power save mode 1: True
	uint8_t power_mon_fail;	//!< Power monitor failed 1: True
};

struct qubik {
	struct radio hradio;
	struct max17261_conf hmax17261;
	struct power_status power;
	struct ft_storage ft_persist_mem;
	struct qubik_settings settings;
	uint32_t uptime_secs;
	fsm_state_t fsm_state;
	struct qubik_hw_status status;
	uint8_t telemetry_idx;
	uint8_t reason_of_reset;
	uint8_t experiment_req;
	uint8_t experiment_stop;
	uint8_t experiment_completed;
};

int
qubik_init(struct qubik *q);

int
qubik_write_settings(struct qubik *q);

#endif /* QUBIK_H_ */
